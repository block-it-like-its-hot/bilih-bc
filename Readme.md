# Give.Kindly Smart contracts

**Repository provisioned using Truffle framework** 

## Smart Contracts 

**Registration Smart Contract**

*Methods*
1. Register - Methods facilitates user registration. Takes username, email and type as paramaters. Type identifies the role of user. It can be donor, charity, auctioneer & cra. 
2. isPersonRegistered - This is used for checking login credentials. If the user is already registered. 

**Donation Smart Contract**

*Methods*
1. Register - This allows for creating a donation. Takes all the form fields that UI includes for a donation. We maintain all donations in a mapping and we use a global variable for tracking the length of the map. 
2. UpdateStatus - This is method used to update the status for a donation. There are 4 status values for a donation. Read, PickedUp, AuctioneerAssigned and Sold respectively. 
3. updateSoldValue - This is method used when final sale by the auctioneer is made. Value is informed to the donor using a receipt. User dashboard allows donors to download this in a pdf. 
4. getDonation - This method takes index as parameters and gets the respective donation. Its a constant method. 
5. getDonationCount - This is used to loop through all the donations in the mapping variable. Used for the dashboard. 