pragma solidity ^0.4.5;

contract DonationContract {
    event DonationCreated(uint donationsIndex, address userId);
    event SoldValueUpdated(string _status, uint _soldValue, uint donationsIndex);
    event StatusAssigned(string status, uint donationsIndex);
    
    function DonationContract() public {
       donations_Count = 0;
    }
   
   struct DonationStr {
       string category ;
       string readyforpickup;
       string vin;
       address userId;
       string homeAddress;
       string city;
       string charity;
       string status;
       uint value; 
   }

   uint donations_Count;
   
   /* mapping function put into vars below */

   mapping(uint => DonationStr) donations;
    

   function register(string _category, string _readyforpickup, string _vin, string _homeAddress, string _city, string _charity) public returns (bool) {
       donations[donations_Count].category = _category;
       donations[donations_Count].readyforpickup = _readyforpickup;
       donations[donations_Count].vin = _vin;
       donations[donations_Count].userId = msg.sender;
       donations[donations_Count].homeAddress = _homeAddress;
       donations[donations_Count].city = _city;
       donations[donations_Count].charity = _charity;
       donations[donations_Count].status = "Ready";
       
       DonationCreated(donations_Count, msg.sender);
       
       donations_Count++;
     
       return true;
    }

    function getDonation(uint donationsIndex) constant public returns(string, string, string, string, uint, uint) {
        return (donations[donationsIndex].category, donations[donationsIndex].vin, donations[donationsIndex].status, donations[donationsIndex].charity, donations[donationsIndex].value, donationsIndex);
    }

    function getDonationCount() constant public returns(uint) {
        return donations_Count;
    }

    function updateSoldValue(uint donationsIndex, uint _soldValue) public returns(uint) {
        donations[donationsIndex].value = _soldValue;    
        donations[donationsIndex].status = "sold";
   
        SoldValueUpdated("sold", _soldValue, donationsIndex);

        return donationsIndex;
    }

    function updateStatus(string _status, uint donationsIndex) public returns (bool) {
        donations[donationsIndex].status = _status;
        StatusAssigned(_status, donationsIndex);
        return true;
    }
}