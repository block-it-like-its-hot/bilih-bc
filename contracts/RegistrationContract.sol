pragma solidity ^0.4.15;

contract RegistrationContract {

    /* Events - START */
    // Indexed arguments in a event are made filterable in the user interface.
    // Only up to 3 indexed arguments are allowed.
    event MainDevChanged(address indexed oldMainDev, address indexed newMainDev);
    event PersonRegistered(address indexed account, string name);
    /* Events - END */

    struct Person {
        string name;
        address account;
        string email;
        string actorType;
        bool registered;
        uint lastTimeSaidHi;
    }
    /* Enum & Strut - END */

    /* State Variables - START */
    // Constant state variable that cannot be changed later.
    
    address public mainDev;
    // Mapping from addresses to struct Person.
    mapping (address => Person) public people;

    uint private creationTime;
    /* State Variables - END */

    /* Modifiers - START */
    modifier onlyMainDev {
        // Require if the shortcut for if (...) throw. Here, we 
        // are restricting the message sender to be the mainDev
        // and throw otherwise which will revert all state changes.
        require(msg.sender == mainDev);

        // _; is the placeholder for where the function codes will be inserted in.
        // Note: more codes can be placed after _; in valid use case as a post-process.
        _;
    }

    modifier onlyRegisteredPerson { 
        require(people[msg.sender].registered);
        _;
    }
    /* Modifiers - END */

    /* Constructor - START */
    function RegistrationContract () public {
        // msg.sender is the direct sender of this message, but not necessarily 
        // the original sender of the transaction. mainDev will be the creator
        // of this smart contract in the beginning.
        mainDev = msg.sender;
        // now is an alias for block.timestamp, which is current block timestamp 
        // as seconds since unix epoch.
        creationTime = now;
    }
    /* Constructor - END */

    /* Public Functions - START */
    /** @dev Change the mainDev. Only accessible by the mainDev.
      *
      * @param _newMainDev Address of the new mainDev.
      *
      * @return true if execution succeeded, false otherwise.
      */
    function changeMainDev(address _newMainDev) onlyMainDev public returns(bool) {
        address _oldMainDev = mainDev;
        mainDev = _newMainDev;

        // This is how we log a specific event in solidity.
        // Note that 'this' in the line below refers to the address of this smart
        // contract. This variable is not available in the constructor though as 
        // the contract has not been deployed yet and has not acquired an address.
        MainDevChanged(_oldMainDev, _newMainDev);

        return true;
    }

    /** @dev Register a new person. Only accessible by the mainDev.
      *
      * @param _name Name of the person.
      * ...
      * ...
      *
      * @return true if execution succeeded, false otherwise.
      */
    function register(string _name, string _email, string _actorType) public returns(bool) {
        // Set each individual field in the target Person struct. Since people is
        // a mapping from addresses to Person, the struct needs not to be manually
        // initialized.
        people[msg.sender].name = _name;
        people[msg.sender].email = _email;
        people[msg.sender].actorType = _actorType;
        people[msg.sender].registered = true;
        people[msg.sender].account = msg.sender;

        PersonRegistered(msg.sender, _name);

        return true;
    }

    

    /* Constant Function - START */
    /** @return true if the given address is registered, false otherwise.
      */
    function isPersonRegistered(address _account) public constant returns(bool) {
        return people[_account].registered;
    }
    /* Constant Function - END */
}
